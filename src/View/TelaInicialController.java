/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaInicialController implements Initializable {
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrarProduto() {
        Main.trocaTela("TelaCadastroProduto.fxml");
    }

    @FXML
    private void realizarVenda() {
        Main.trocaTela("TelaRealizarVenda.fxml");
    }

    @FXML
    private void manipularEstoque() {
        Main.trocaTela("TelaEstoque.fxml");
    }

    @FXML
    private void relatorioDeVendas() {
        Main.trocaTela("TelaRelatorioVendas.fxml");
    }
    
}
