/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaRealizarVendaController implements Initializable {

    @FXML
    private TableView<Produto> tabelaProd;
    @FXML
    private TableColumn<?, ?> nomeProd;
    @FXML
    private TableColumn<?, ?> precoProd;
    @FXML
    private TableColumn<?, ?> estoqueProd;
    
    @FXML
    private TableView<ItemNoCarrinho> tabelaCarrinho;
    @FXML
    private TableColumn<?, ?> produtoCarrinho;
    @FXML
    private TableColumn<?, ?> quantCarrinho;
    
    @FXML
    private Label precoFinal;
    
    private Venda v;
    
    private ArrayList<ItemNoCarrinho> car = new ArrayList<>();
    @FXML
    private TableColumn<?, ?> validade;
    @FXML
    private TableColumn<?, ?> extra;
    @FXML
    private Button addCarrinho;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        v = new Venda();
        criarColunas();
        criarLinhasProd();
        this.calculaVenda();
    }    

    @FXML
    private void addCarrinho() {
        Produto pr = tabelaProd.getSelectionModel().getSelectedItem();
        boolean jEst = true;
        
        if(pr != null){
            for(ItemNoCarrinho i : car){
                if(i.getNome().equals(pr.getNome())){
                    if(i.getQuantidade()+1<=pr.getQunt()){
                        i.setQuantidade(i.getQuantidade()+1);
                    }
                    tabelaCarrinho.refresh();
                    jEst = false;
                }
            }
            
            if(jEst){
                car.add(new ItemNoCarrinho(pr));
                tabelaCarrinho.getItems().clear();
                criarLinhasCar();
                this.v.addItens(pr);
            }
        }
        else{
            System.out.println("HUMAN AFTER ALL!!");
        }
        this.calculaVenda();
    }
    
    @FXML
    private void deleteCarrinho() {
        ItemNoCarrinho pr = tabelaCarrinho.getSelectionModel().getSelectedItem();
        
        if(pr!=null){
            if(pr.getQuantidade()-1==0){
                tabelaCarrinho.getItems().remove(pr);
                this.car.remove(pr);
                v.getItens().remove(pr.getP());
            }
            else{
               for(ItemNoCarrinho i : car){
                   if(i.equals(pr)){
                       i.setQuantidade(i.getQuantidade()-1);
                   }
               }
               
               tabelaCarrinho.refresh();
            }
        }
        
        this.calculaVenda();
    }

    @FXML
    private void concluirVenda() {
        int genId = v.insert();
        for(ItemNoCarrinho i : this.car){
            i.getP().setQunt(i.getP().getQunt()-i.getQuantidade());
            i.getP().update();
            i.insert(genId);
        }
        
        tabelaProd.refresh();
        tabelaCarrinho.getItems().clear();
        car.clear();
        v.getItens().clear();
        this.calculaVenda();
        
        v = new Venda();
    }

    @FXML
    private void voltar() {
        Main.trocaTela("TelaInicial.fxml");
    }
    
    public void criarColunas(){
        nomeProd.setCellValueFactory(new PropertyValueFactory<>("nome"));
        precoProd.setCellValueFactory(new PropertyValueFactory<>("preco"));
        validade.setCellValueFactory(new PropertyValueFactory<>("prazo"));
        extra.setCellValueFactory(new PropertyValueFactory<>("extra"));
        estoqueProd.setCellValueFactory(new PropertyValueFactory<>("qunt"));
        
        produtoCarrinho.setCellValueFactory(new PropertyValueFactory<>("nome"));
        quantCarrinho.setCellValueFactory(new PropertyValueFactory<>("quantidade"));
    }
    
    public void criarLinhasProd(){
        for(Produto p : Produto.getAll()){
            if(p.getQunt()>0){
                tabelaProd.getItems().add(p);
            }
        }
    }
    
    public void criarLinhasCar(){
        for(ItemNoCarrinho i : this.car){
            tabelaCarrinho.getItems().add(i);
        }
    }
    
    private void calculaVenda(){
        double d=0;
        
        for(ItemNoCarrinho i : this.car){
            d+=(i.getQuantidade()*i.getP().getPreco());
        }
        d = ((d*100)/100);
        this.v.setPrecoFinal(d);
        precoFinal.setText("R$"+this.v.getPrecoFinal());
    }
}