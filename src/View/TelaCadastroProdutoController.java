
package View;

import Model.*;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;


public class TelaCadastroProdutoController implements Initializable {

    @FXML
    private TextField nome;
    @FXML
    private TextField qunt;
    @FXML
    private TextField preco;
    @FXML
    private Button enviarButton;
    @FXML
    private Label prazoLabel;
    @FXML
    private RadioButton duravel;
    @FXML
    private RadioButton limpeza;
    @FXML
    private RadioButton frutas;
    @FXML
    private TextField prazoTF;
    
    private int o;
    @FXML
    private TextField extra;
    @FXML
    private Label extraLabel;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void voltar() {
        Main.trocaTela("TelaInicial.fxml");
    }

    @FXML
    private void cadastrarProd() {
        Produto p;
        switch(o){
            case 1:
                p = new Duravel(prazoTF.getText());
                p.setTipo("Duravel");
                break;
            case 2:
                p = new Limpeza(prazoTF.getText(),extra.getText());
                p.setTipo("Limpeza");
                break;
            default:
                p = new Comida(prazoTF.getText(),extra.getText());
                p.setTipo("Comida");
            
        }
        
        p.setNome(nome.getText());
        p.setPreco(Double.parseDouble(preco.getText()));
        p.setQunt(Integer.parseInt(this.qunt.getText()));
        
        p.insert();
        
        prazoTF.setText("");
        extra.setText("");
        nome.setText("");
        qunt.setText("");
        preco.setText("");
        prazoTF.setVisible(false);
        prazoLabel.setVisible(false);
        extraLabel.setVisible(false);
        extra.setVisible(false);
        duravel.setSelected(false);
        limpeza.setSelected(false);
        frutas.setSelected(false);
        enviarButton.setDisable(true);
    }

    @FXML
    private void isSelecionado() {
        if(duravel.isSelected()){
            o=1;
            mostra(false,"Garantia:");
            prazoTF.setPromptText("Ex: 1 ano");
        }
        else if(limpeza.isSelected()){
            o=2;
            prazoTF.setPromptText("dd/mm/aaaa");
            mostra(true,"Prazo de validade:");
            extraLabel.setText("Nome da marca:");
        }
        else if(frutas.isSelected()){
            o=3;
            prazoTF.setPromptText("dd/mm/aaaa");
            mostra(true,"Prazo de validade:");
            extraLabel.setText("Sabor:");
        }
    }
    
    private void mostra(boolean v, String s){
        prazoLabel.setVisible(true);
        prazoLabel.setText(s);
        prazoTF.setVisible(true);
        
        enviarButton.setDisable(true);
        
        extraLabel.setVisible(v);
        extra.setVisible(v);
        extra.setText("");
    }

    @FXML
    private void escrito() {
        if(!prazoTF.getText().equals("")){
            enviarButton.setDisable(false);
        }
        else{
            enviarButton.setDisable(true);
        }
    }
}
