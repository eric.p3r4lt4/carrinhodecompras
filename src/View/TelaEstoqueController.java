/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Produto;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Cliente
 */
public class TelaEstoqueController implements Initializable {

    @FXML
    private TableView<Produto> tabela;
    @FXML
    private TableColumn<?, ?> idTab;
    @FXML
    private TableColumn<?, ?> nomeTab;
    @FXML
    private TableColumn<?, ?> precoTab;
    @FXML
    private TableColumn<?, ?> estoqueTab;
    @FXML
    private TextField addTF;
    @FXML
    private TextField minusTF;
    @FXML
    private TextField changePreco;
    @FXML
    private Label aviso;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        criarColunas();
        criarLinhas();
    }    

    @FXML
    private void addEstoque() {
        Produto p = tabela.getSelectionModel().getSelectedItem();
        int m = Integer.parseInt(addTF.getText());
        
        if(testaNull(p)){
            p.setQunt(p.getQunt()+m);
            p.update();
            tabela.refresh();
        }
        
        addTF.setText("");
    }

    @FXML
    private void minusEstoque() {
        Produto p = tabela.getSelectionModel().getSelectedItem();
        int m = Integer.parseInt(minusTF.getText());
        
        if(testaNull(p)){
            if(p.getQunt()-m>=0){
                p.setQunt(p.getQunt()-m);
            }
            
            else{
                p.setQunt(0);
            }
            p.update();
            tabela.refresh();
        }
        
        minusTF.setText("");
    }

    @FXML
    private void mudarPreco() {
        Produto p = tabela.getSelectionModel().getSelectedItem();
        
        if(testaNull(p)){
            p.setPreco(Double.parseDouble(changePreco.getText()));
            p.update();
            tabela.refresh();
        }
        
        changePreco.setText("");
    }

    @FXML
    private void Voltar() {
        Main.trocaTela("TelaInicial.fxml");
    }
    
    private void criarColunas(){
        idTab.setCellValueFactory(new PropertyValueFactory<>("id"));
        nomeTab.setCellValueFactory(new PropertyValueFactory<>("nome"));
        precoTab.setCellValueFactory(new PropertyValueFactory<>("preco"));
        estoqueTab.setCellValueFactory(new PropertyValueFactory<>("qunt"));
    }
    
    private void criarLinhas(){
        for(Produto p : Produto.getAll()){
            tabela.getItems().add(p);
        }
    }
    
    private boolean testaNull(Produto p){
        boolean t = true;
        
        if(p==null){
            aviso.setText("Selecione alguma linha!");
            t = false;
        }
        
        return t;
    }
}
