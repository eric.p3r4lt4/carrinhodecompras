/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Venda;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Cliente
 */
public class TelaRelatorioVendasController implements Initializable {

    @FXML
    private TableView<Venda> tabela;
    @FXML
    private TableColumn<?, ?> idVenda;
    @FXML
    private TableColumn<?, ?> prodVenda;
    @FXML
    private TableColumn<?, ?> precoTab;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.criarColunas();
        this.criarLinhas();
    }    

    @FXML
    private void voltar() {
        Main.trocaTela("TelaInicial.fxml");
    }
    
    private void criarColunas(){
        idVenda.setCellValueFactory(new PropertyValueFactory<>("id"));
        prodVenda.setCellValueFactory(new PropertyValueFactory<>("produtos"));
        precoTab.setCellValueFactory(new PropertyValueFactory<>("precoFinal"));
    }
    
    private void criarLinhas(){
        for(Venda v : Venda.getAll()){
            tabela.getItems().add(v);
        }
    }
}
