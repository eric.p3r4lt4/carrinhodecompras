/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public abstract class Produto {

    private int id;
    private String nome;
    private Double preco;
    private String tipo;
    private int qunt;
    
    public abstract String getExtra();
    public abstract void extraInsert();
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQunt() {
        return qunt;
    }

    public void setQunt(int qunt) {
        this.qunt = qunt;
    }
    //CRUD-------------------------
    
    public void insert() {
        Conexao c = new Conexao("dbds3_09", "123456");
        PreparedStatement ps;
        String[] co = {"id"};

        try {
            ps = c.getConexao().prepareStatement("insert into produto values(prod_seq.nextval,?,?,?,?)",co);
            ps.setString(1, this.nome);
            ps.setDouble(2, this.qunt);
            ps.setString(3, this.tipo);
            ps.setDouble(4, this.preco);
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next()){
                this.id = rs.getInt(1);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        this.extraInsert();
    }

    public void update() {
        Conexao c = new Conexao("dbds3_09", "123456");
        PreparedStatement ps;

        try {
            ps = c.getConexao().prepareStatement("update produto set qunt=?,preco=? where id = ?");
            ps.setInt(3, this.id);

            ps.setInt(1, this.qunt);
            ps.setDouble(2, this.preco);
            ps.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }

    public static ArrayList<Produto> getAll() {
        
        ArrayList<Produto> lista = new ArrayList<Produto>();

        Conexao c = new Conexao("dbds3_09", "123456");
        Statement s;

        try {
            s = c.getConexao().createStatement();
            ResultSet rs = s.executeQuery("select * from produto inner join produto_extras on produto_extras.id = produto.id");
            
            while (rs.next()) {
                
                Produto p = null;
                
                switch (rs.getString("tipo")) {
                    case "Comida":
                            p = new Comida(String.valueOf(rs.getDate("prazo")),rs.getString("Sabor"));
                            p.setTipo("Comida");
                            
                        break;
                    case "Limpeza":
                            p = new Limpeza(String.valueOf(rs.getDate("prazo")),rs.getString("marca"));
                            p.setTipo("Limpeza");
                        break;
                    default:
                            p = new Duravel(rs.getString("garantia"));
                            p.setTipo("Duravel");
                        break;
                }
                
                
                p.setNome(rs.getString("nome"));
                p.setPreco(rs.getDouble("preco"));
                p.setId(rs.getInt("id"));
                p.setQunt(rs.getInt("qunt"));
                lista.add(p);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }
    
    public static Produto getOne(int id){
        Conexao c = new Conexao("dbds3_09","123456");
        PreparedStatement ps;
        Produto p = null;

        try {
            ps = c.getConexao().prepareStatement("select * from produto inner join produto_extras on produto.id = produto_extras.id where produto.id = ?");
            ps.setInt(1,id);
            
            
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                switch (rs.getString("tipo")) {
                    case "Comida":
                            p = new Comida(String.valueOf(rs.getDate("prazo")),rs.getString("Sabor"));
                            p.setTipo("Comida");
                            
                        break;
                    case "Limpeza":
                            p = new Limpeza(String.valueOf(rs.getDate("prazo")),rs.getString("marca"));
                            p.setTipo("Limpeza");
                        break;
                    default:
                        p = new Duravel(rs.getString("garantia"));
                        p.setTipo("NaoConsumivel");
                        break;
                }
                
                
                p.setNome(rs.getString("nome"));
                p.setPreco(rs.getDouble("preco"));
                p.setId(rs.getInt("id"));
                p.setQunt(rs.getInt("qunt"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return p;
    }
}