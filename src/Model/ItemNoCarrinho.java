package Model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ItemNoCarrinho {
    private Produto p;
    private int quantidade = 1;
    
    public ItemNoCarrinho(Produto p){
        this.setP(p);
    }

    public Produto getP() {
        return p;
    }

    public void setP(Produto p) {
        this.p = p;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public String getNome(){
        return this.p.getNome();
    }
    
    public void insert(int idVenda){
        Conexao c = new Conexao("dbds3_09","123456");
        PreparedStatement ps;
        
        try{
            ps = c.getConexao().prepareStatement("insert into produto_venda values(?,?,?)");
            ps.setInt(1, idVenda);
            ps.setInt(2, this.p.getId());
            ps.setInt(3, this.quantidade);
            
            ps.executeUpdate();
        }   
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
}