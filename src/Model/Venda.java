/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Venda {
    private int id;
    private double precoFinal;
    private ArrayList<Produto> itens = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrecoFinal() {
        return precoFinal;
    }

    public void setPrecoFinal(double precoFinal) {
        this.precoFinal = precoFinal;
    }

    public ArrayList<Produto> getItens() {
        return itens;
    }

    public void setItens(ArrayList<Produto> itens) {
        this.itens = itens;
    }
    
    public void addItens(Produto p){
        this.itens.add(p);
    }
    
    public String getProdutos(){
        String f = "";
        Conexao c = new Conexao("dbds3_09","123456");
        PreparedStatement ps;
        
        for(Produto i : this.itens){
            try{
                ps = c.getConexao().prepareStatement("select qunt from produto_venda where idvenda = ? and idproduto = ?");
                ps.setInt(1,this.id);
                ps.setInt(2,i.getId());
                
                ResultSet rs = ps.executeQuery();
            
                if(rs.next()){
                    int q = +rs.getInt("qunt");
                    f+=(q+" "+i.getNome()+"\n");
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        
        c.desconecta();
        return f;
    }
    
    //CRUD-----------------
    public int insert(){
        Conexao c = new Conexao("dbds3_09","123456");
        PreparedStatement ps;
        String newId[] = {"id"};
        int genId = 0;
        
        try{
            ps = c.getConexao().prepareStatement("insert into venda(id,preco) values(venda_seq.nextval,?)",
                                                    newId);
            
            ps.setDouble(1, this.precoFinal);
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next()){
                genId = rs.getInt(1);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return genId;
    }
    
    public static ArrayList<Venda> getAll() {
        ArrayList<Venda> lista = new ArrayList<Venda>();

        Conexao c = new Conexao("dbds3_09", "123456");
        Statement s;
        PreparedStatement ps;

        try {
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery("select * from venda");
            ResultSet rs2;
            
            
            while (rs.next()) {
                Venda p = new Venda();
                
                p.setId(rs.getInt("id"));
                p.setPrecoFinal(rs.getDouble("preco"));
                
                ps = c.getConexao().prepareStatement("select idproduto from produto_venda where idvenda = ?");
                ps.setInt(1, p.getId());
                rs2 = ps.executeQuery();
                
                while(rs2.next()){
                    p.addItens(Produto.getOne(rs2.getInt("idProduto")));
                }
                
                lista.add(p);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }
}
