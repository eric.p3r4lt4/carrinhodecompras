/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Limpeza extends NaoDuravel {
    private String marca;
    public Limpeza(String date,String marca) {
        super(date);
        this.marca = marca;
    }
    
    public void setMarca(String marca){
        this.marca = marca;
    }

    @Override
    public String getExtra() {
    return "Marca: " + this.marca;
    }
    
    @Override
    public void extraInsert() {
        Conexao c = new Conexao("dbds3_09","123456");
        PreparedStatement ps;
        
        try{
            ps = c.getConexao().prepareStatement("insert into produto_extras values(?,?,?,?,?)");
            ps.setInt(1,this.getId());
            ps.setDate(2,this.prazo);
            ps.setString(3,this.marca);
            ps.setString(4,null);
            ps.setString(5, null);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
}
