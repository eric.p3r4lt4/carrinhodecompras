/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Duravel extends Produto{
    private String garantia;
    public Duravel(String garantia){
        this.garantia = garantia;
    }

    @Override
    public String getExtra() {
        return "Garantia: " + this.garantia;
    }
    
    @Override
    public void extraInsert() {
        Conexao c = new Conexao("dbds3_09","123456");
        PreparedStatement ps;
        
        try{
            ps = c.getConexao().prepareStatement("insert into produto_extras values(?,?,?,?,?)");
            ps.setInt(1,this.getId());
            ps.setString(2,null);
            ps.setString(3,null);
            ps.setString(4,this.garantia);
            ps.setString(5, null);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
}
