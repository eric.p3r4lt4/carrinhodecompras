package Model;

import java.sql.Date;

public abstract class NaoDuravel extends Produto{
    protected Date prazo;
    
    public NaoDuravel(String data){
        this.setPrazo(data);
    }
    
    public void setPrazo(String data){
        String[] date = data.split("/");
        
        if(date.length == 3){
            data = "";
            
            data += date[2] + "-" + date[1] + "-" + date[0];
            
            this.prazo = Date.valueOf(data);
        }
        
        else if(data.split("-").length == 3){
            this.prazo = Date.valueOf(data);
        }
        
        else{
            this.prazo = Date.valueOf("0000-00-00");
        }
    }
    
    /**
     *
     * @return
     */
    public String getPrazo(){
        String[] s = this.prazo.toString().split("-");
        String t = "";
        
        t+=(s[2] + "/" + s[1] + "/" +s[0]);
        
        return t;
    }
}
